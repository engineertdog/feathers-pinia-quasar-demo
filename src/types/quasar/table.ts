export interface IPagination {
  sortBy: string;
  descending: boolean;
  page: number;
  rowsPerPage: number;
  rowsNumber?: number;
}

export class Pagination implements IPagination {
  sortBy: string;
  descending: boolean;
  page: number;
  rowsPerPage: number;
  rowsNumber?: number;

  constructor(sortBy: string) {
    this.sortBy = sortBy;
    this.descending = false;
    this.page = 1;
    this.rowsPerPage = 20;
    this.rowsNumber = 0;
  }
}
