export type ISidebarComponentType = "separator" | "link" | "title";

export interface ISidebarComponent {
  type: ISidebarComponentType;
  title?: string;
  icon?: string;
  caption?: string;
  routeName?: string;
}

export interface INavbarSearchOptions {
  label: string;
  routeName: string;
}

export interface INavbarSearchFilteredOptions extends INavbarSearchOptions {
  type: string;
}
