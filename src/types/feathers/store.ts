export interface IFeathersStore {
  sortBy: string;
  sortDirection?: number;
}
