import { RouteRecordRaw } from "vue-router";
import routeNames from "./routeNames";

const routes: RouteRecordRaw[] = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "",
        name: routeNames.dashboard,
        component: () => import("pages/IndexPage.vue")
      }
    ]
  },
  {
    path: "/login",
    name: routeNames.login,
    component: () => import("pages/account/Login.vue")
  },

  // Always leave this as the last route, but you can also remove it.
  {
    path: "/:catchAll(.*)*",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "",
        name: routeNames.error,
        component: () => import("pages/ErrorNotFound.vue")
      }
    ]
  }
];

export default routes;
