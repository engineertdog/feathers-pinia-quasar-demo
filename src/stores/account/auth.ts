import { defineStore } from "pinia";
import { useAuth } from "feathers-pinia";
import { useUserStore } from "./users";
import { useFeathers } from "boot/feathers";

export const useAuthStore = defineStore("auth", () => {
  const { userStore } = useUserStore();
  const { $api } = useFeathers();

  const auth = useAuth({
    api: $api,
    userStore
  });

  return auth;
});
