import { pinia, useFeathers, defineStore, BaseModel } from "src/boot/feathers";

// create a data model
export class User extends BaseModel {
  _id: string;
  email: string;
  firstName: string;
  lastName: string;
  fullName: string;

  // Minimum required constructor
  constructor(data: Partial<User> = {}, options: Record<string, User> = {}) {
    super(data, options);
    this.init(data);
  }
}

export const useUserStore = () => {
  const { $api } = useFeathers();
  const servicePath = "account/users";

  const useStore = defineStore({
    servicePath,
    Model: User,
    state() {
      return {};
    },
    getters: {},
    actions: {}
  });

  const store = useStore(pinia);

  $api.service(servicePath).hooks({});

  return {
    userStore: store,
    User: User as typeof store.Model
  };
};
