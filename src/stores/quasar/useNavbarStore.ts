import { defineStore } from "pinia";
import { ref } from "vue";

export const useNavbarStore = defineStore("quasarNavbar", () => {
  const leftDrawerOpen = ref(false);
  const toggleLeftDrawer = () => {
    leftDrawerOpen.value = !leftDrawerOpen.value;
  };

  return {
    leftDrawerOpen,
    toggleLeftDrawer
  };
});
