import { api, defineStore, BaseModel } from "src/boot/feathers";
import { QTableProps } from "quasar";
import { User } from "src/stores/account/users";

export class Domain extends BaseModel {
  _id: string;
  domain: string;
  description: string;
  createdAt: Date;
  createdBy: string;
  createdByUser?: User;
  updatedAt: Date;
  updatedBy: string;
  updatedByUser?: User;
}

const servicePath = "system/domains";
export const useDomainStore = defineStore({ servicePath, Model: Domain });

api.service(servicePath).hooks({});

export const columns: QTableProps["columns"] = [
  {
    name: "_id",
    label: "ID",
    field: "_id",
    align: "left"
  },
  {
    name: "name",
    label: "Domain Name",
    field: "domain",
    align: "left",
    sortable: true
  },
  {
    name: "description",
    label: "Description",
    field: "description",
    align: "left"
  },
  {
    name: "updatedBy",
    label: "Updated By",
    field: (row: Domain) => row.updatedByUser?.fullName,
    align: "left",
    sortable: true
  }
];
