import { createPinia } from "pinia";
import { setupFeathersPinia } from "feathers-pinia";
import { feathers } from "@feathersjs/feathers";
import socketio from "@feathersjs/socketio-client";
import auth from "@feathersjs/authentication-client";
import io from "socket.io-client";

const socket = io("http://localhost:3030", { transports: ["websocket"] });
export const api = feathers()
  .configure(socketio(socket))
  .configure(auth({ storage: window.localStorage }));

export const { defineStore, BaseModel } = setupFeathersPinia({
  clients: { api },
  idField: "id"
});

export const useFeathers = () => {
  return { $api: api };
};

export const pinia = createPinia();
