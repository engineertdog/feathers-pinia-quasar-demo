import { ref, reactive } from "vue";
import { QTableProps } from "quasar";
import { FindFn, BaseModel, FindClassParams } from "feathers-pinia";
import { IFeathersStore, Pagination } from "src/types";

export const useFeathersFindStore = ({
  sortBy = "_id",
  sortDirection = 1
}: IFeathersStore) => {
  const loading = ref(false);
  const pagination = ref(new Pagination(sortBy));
  const requestPagination = ref(new Pagination(sortBy));
  const storeParams = reactive<FindClassParams>({
    query: {
      $limit: 20,
      $skip: 0,
      $sort: {
        [sortBy]: sortDirection
      }
    },
    paginate: true,
    onServer: false,
    immediate: false
  });

  const setPagination = (limit: number, total: number, currentPage: number) => {
    pagination.value = Object.assign(
      {},
      {
        sortBy: requestPagination.value.sortBy,
        descending: requestPagination.value.descending,
        rowsPerPage: limit,
        rowsNumber: total,
        page: currentPage
      }
    );
  };

  const setPaginationParams: QTableProps["onRequest"] = (requestProps) => {
    requestPagination.value = requestProps.pagination;
    const {
      sortBy: sort,
      descending,
      rowsPerPage,
      page
    } = requestProps.pagination;

    storeParams.query = Object.assign(storeParams.query, {
      $limit: rowsPerPage,
      $skip: (page - 1) * rowsPerPage,
      $sort: {
        [sort]: descending ? -1 : 1
      }
    });

    // "All" in Quasar table means removing limit and skip for Feathers.
    if (rowsPerPage === 0) {
      delete storeParams.query.$limit;
      delete storeParams.query.$skip;
    }
  };

  const setLoading = (value: boolean) => {
    loading.value = value;
  };

  const getRecords = async (
    find: FindFn<BaseModel>,
    limit: number,
    total: number,
    currentPage: number
  ) => {
    setLoading(true);

    try {
      await find();
      setPagination(limit, total, currentPage);
    } catch (err: unknown) {
      console.log("failed", err);
    }

    setLoading(false);
  };

  return {
    storeParams,
    pagination,
    loading,
    getRecords,
    setPagination,
    setPaginationParams,
    setLoading
  };
};
